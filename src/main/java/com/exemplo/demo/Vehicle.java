package com.exemplo.demo;


public interface Vehicle {
	
	public void start();
	
	public void stop();

}
